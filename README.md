# Object Orianted Programming
Gebze Technical University 2018 Fall Object Orianted Programming 

This repository includes C++ and Java projects.

Homework 1,2,3,4,5 are the same project. Each project has additional OOP upgrades.

Homework 1 -> 2D Shapes without OOP

Homework 2 -> Classes

Homework 3 -> Operator Overloading

Homework 4 -> Dynamic Memory and Name Spaces

Homework 5 -> Inheritance

Homework 6 -> Templates and STL

Homework 7 -> First Java Program

Homework 8 -> Generics and Collections in Java

